// Project skeleton generated with vep.
// vep v0.2 (2019.03.03) - See https://gitlab.com/mszmurlo/vep - MIT License

package org.buguigny.mongotest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.Future;

import io.vertx.ext.mongo.MongoClient;
import io.vertx.core.json.JsonObject;
 
public class Main {

  private Vertx vertx = null;
  private MongoClient mc = null;
  private static final Logger log = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {
    (new Main()).startup(args);
  }

  private void startup(String[] args) {
    VertxOptions opts = new VertxOptions();
    vertx = Vertx.vertx(opts);

    log.info("Connecting to Mongo Server");
    JsonObject mcnf = new JsonObject()
      .put("pool_name", "mongodb") 
      .put("host", "localhost") 
      .put("port", 27017)
      .put("db_name", "app")
      .put("username", "app-user")
      .put("password", "app-passwd")
      .put("authSource", "admin")
      .put("maxPoolSize", 5)
      .put("minPoolSize", 1)
      .put("useObjectId", false)
      ;
    mc = MongoClient.createShared(vertx, mcnf, mcnf.getString("pool_name"));
     

    log.info("Starting HTTP server");
    HttpServer server = vertx.createHttpServer();
    Router router = Router.router(vertx);        
    router.get("/save/:fn/:ln").handler(this::saveHandler);
    router.get("/demo/:id").handler(this::demoHandler);
    router.route().handler(this::defHandler); 
    server
      .requestHandler(router::accept)
      .listen(8080, ar -> {
        if(ar.succeeded()) {
          log.info("Server running on port 8080");
	}
        else {
          log.error("Could not start server on port 8080");
          System.exit(-1);
        }
      });
  }
    
  private void saveHandler(RoutingContext rc) {
    String fn = rc.request().getParam("fn");
    String ln = rc.request().getParam("ln");
    log.info("Got a 'save' request with parameters fn={} ln={}", fn, ln);
    JsonObject user = new JsonObject().put("fn", fn).put("ln", ln);
    log.info("user to save : {}", user.encode());
    saveUser(user).setHandler(ar -> {
    	if(ar.succeeded()) {
	  log.debug("Saved user {}", user.encode());
	  rc.response().setStatusCode(200).end("OK: id = "+ar.result()+"\n");
	}
	else {
	  log.error("Failed to save the user {}", user.encode());
	  rc.response().setStatusCode(200).end("KO\n");
	}
      });
  }

  
  private void demoHandler(RoutingContext rc) {
    String id = rc.request().getParam("id");
    log.info("Got a 'load' request with parameters id={}", id);
    loadUser(id).setHandler(ld_ar -> {
    	if(ld_ar.succeeded()) {
    	  JsonObject user = ld_ar.result();
	  log.debug(">>> Loaded user '{}'", user.encode());
	  saveUser(user).setHandler(s_ar -> {
	      if(s_ar.succeeded()) {
		log.debug(">>> Saved user '{}'", user.encode());
		rc.response().setStatusCode(200).end("OK\n");
	      }
	      else {
		log.error("Failed to save user {}", user.encode());
		rc.response().setStatusCode(200).end("KO\n");
	      }
	    });
	}
	else {
	  log.error("Failed to load user {}", id);
	  rc.response().setStatusCode(200).end("KO\n");
	}
      });
  }
    
    

  private void defHandler(RoutingContext rc) {
    log.warn("Got a request '{} {}'. Not permitted.",
	     rc.request().method(), rc.request().absoluteURI());
    rc.response()
      .setStatusCode(404)
      .end();
  }
  
  private Future<String> saveUser(JsonObject user) {
    Future<String> fut = Future.future();
    mc.save("users", user, ar -> { //.copy(), ar -> {
	if(ar.succeeded()) {
	  // log.debug("Successfully saved user '{}'", user.encode());
  	  fut.complete(ar.result());
  	}
  	else {
  	  log.error("Failed to write user in db. Cause: {}", ar.cause());
  	  fut.fail(ar.cause());
  	}
      });
    return fut;
  }
  
  private Future<JsonObject> loadUser(String id) {
    Future<JsonObject> fut = Future.future();
    JsonObject query = new JsonObject().put("_id", id);
    mc.find("users", query, ar -> {
  	if(ar.succeeded()) {
          if(ar.result().size() == 0) {
            log.debug("User with _id='{}' not found", id);
            fut.fail("user not found");
          }
          else {
            // log.debug("Found user with _id='{}'", id);
            fut.complete(ar.result().get(0));
          }
        }
        else {
  	  log.error("Failed to retrieve user with id='{}'. DB Poblem ?. Cause: {}",
  		    id, ar.cause().getMessage());
  	  fut.fail(ar.cause());
  	}
      });
    return fut;
  }
  
}
